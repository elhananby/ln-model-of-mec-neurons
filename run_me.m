%% Description of run_me


% This script is segmented into several parts. First, the data (an
% example cell) is loaded. Then, 15 LN models are fit to the
% cell's spike train. Each model uses information about
% position, head direction, running speed, theta phase,
% or some combination thereof, to predict a section of the
% spike train. Model fitting and model performance is computed through
% 10-fold cross-validation, and the minimization procedure is carried out
% through fminunc. Next, a forward-search procedure is
% implemented to find the simplest 'best' model describing this spike
% train. Following this, the firing rate tuning curves are computed, and
% these - along with the model-derived response profiles and the model
% performance and results of the selection procedure are plotted.

% Code as implemented in Hardcastle, Maheswaranthan, Ganguli, Giocomo,
% Neuron 2017
% V1: Kiah Hardcastle, March 16, 2017

%% Clear the workspace and load the data
dbstop if error;
warning('off','all')
clearvars; close all; clc

files = subdir('D:\experiment_data\Cells\*.mat');
nameField = extractfield(files, 'name');
[~, fileNames, ~] = cellfun(@fileparts, extractfield(files, 'name'), 'un', false);
[~, sortedIdx] = natsortfiles(fileNames);
filesSorted = files(sortedIdx);

fileIdx = 1;
for iiFile = 1:length(filesSorted)
    % load the data
    load(filesSorted(iiFile).name);
    
    [figDir, figName, ~] = fileparts(filesSorted(iiFile).name);
    figFile = fullfile('D:\Scripts\ln-model-of-mec-neurons\results\', ['LN_' figName]);
    
    % calculate occupancy percentage
    binEdges = linspace(0, 96.5, 30);
    occupancyMap = histcounts2(vt.posx_c, vt.posy_c, binEdges, binEdges);
    occupancyPercentage = nnz(occupancyMap)./numel(occupancyMap);
    
    % keep only cells where the session >= 10 minutes and more than 300
    % spikes and occupancy > .5
    if (p.S(metaData.session).end_time - p.S(metaData.session).start_time)*1e-6/60 >= 10 &&...
            length(c.timestamps) >= 300 && occupancyPercentage >= .5
        
        % if we already created a plot, skip all processing
        if exist([figFile '.png'], 'file'), continue; end
        
        % if we haven't done the processing yet
        if ~exist(['D:\Scripts\ln-model-of-mec-neurons\backups\' figName '.mat'], 'file')
            fprintf('Processing file %i / %i\n', iiFile, length(filesSorted));
            
            % keep only data with speed between 2 and 100 cm/s
            sampleRate = 25;

            % organize data
            boxSize = 96.5;
            post = vt.timestamps*1e-6 - vt.timestamps(1)*1e-6;
            posx = vt.posx;
            posx2 = vt.posx2;
            posx_c = vt.posx_c;
            posy = vt.posy;
            posy2 = vt.posy2;
            posy_c = vt.posy_c;
            eeg_sample_rate = 30000;
            spiketrain = spikeTrain;
            
            fprintf('(1/5) Loading data from cell %i \n', metaData.cell_id)
            
            %% fit the model
            fprintf('(2/5) Fitting all linear-nonlinear (LN) models\n')
            fit_all_ln_models
            
            %% find the simplest model that best describes the spike train
            fprintf('(3/5) Performing forward model selection\n')
            select_best_model
            
            %% Compute the firing-rate tuning curves
            fprintf('(4/5) Computing tuning curves\n')
            compute_all_tuning_curves
            
            save(['D:\Scripts\ln-model-of-mec-neurons\backups\' figName]);
        else % if we have done the processing, load the saved file and just plot
            load(['D:\Scripts\ln-model-of-mec-neurons\backups\' figName '.mat']);
        end
        
        %% plot the results
        fprintf('(5/5) Plotting performance and parameters\n')
        plot_performance_and_parameters
    end
end
a